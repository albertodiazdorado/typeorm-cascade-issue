# Issue reproducer
This repository contains a reproducer for an issue with `ManyToOne` relations and `Unique`
constraints in TypeORM.

## Issue description
Let `A` and `B` be two tables in the same database, with a many-to-one between them.
An entity in table `A` is associated with a single entity in table `B`, but entities in table `B` may be associated with as many entities in table `A` as desired.
Let the many-to-one relationship have the option `{ cascade: ["insert"] }`, so that whenever an entity
is inserted in table `A`, the corresponding entity is inserted in table `B` if it does not exist.
* **Expected behaviour**: If I insert an entity in table `A`, and the corresponding entity already
exists in table `B`, no new entry is created in table `B`.
* **Actual behaviour:** If I insert an entity in table `A`, and the corresponding entity already
  exists in table `B`, the entry in table `B` is duplicated.

## Reproducer detailed explanation
Take the two repositories `User` and `Color`.
There is a `many-to-one` relationship between `color`s and `user`: many `user`s may have the same favourite `color`.
However, there is a caveat: `color`s are unique, and they are uniquely identified by their name.
That is why we introduce the constraint `Unique` in the `Color` repository.

On the other hand, we do not want to create `user`s and `color`s separately.
For that, we add the option `{ cascade: ["insert"] }` to the `@ManyToOne` decorator in `User`.
This means that, whenever we add a new `user` to our database, his favourite `color` will be added
to the `Color` repository if it does not exist yet.

The use case described above works as expected.
This is demonstrated in the first part of the test, where we add a new user "Alfred" whose
favourite color is "red".
"Alfred" gets added to the `User` repository, and "red" is added to the `Color` repository.

However, it does not work if we try to add a second user whose favourite color is also "red".
In the second part of the test, we try to add the user "Berta", whose favourite color is "red".
The expected behaviour is that no entry is needed in the `Color` table, since the entry for
"red" already exists.
Nonetheless, TypeORM does not realise that "red" already exists and tries to insert it again.
This results in an error, since the color `name` column has a uniqueness constraint.


## Reproduce the issue
1. Download the repository
```shell
git clone https://gitlab.com/albertodiazdorado/typeorm-cascade-issue.git
```

2. Install the dependencies
```shell
yarn install    # alternatively, npm install
```

3. Run tests
```shell
yarn test       # alternatively, npm run test
```

**Expected result:** The test is green

**Actual result:** The test is red

