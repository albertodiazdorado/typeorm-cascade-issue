import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity({ name: "color" })
@Unique(["color"])
export class Color {
    @PrimaryGeneratedColumn({ name: "color_id", type: "int" })
    id!: number;

    @Column({ type: "varchar", length: 50 })
    color!: string;

    @Column({ type: "datetime" })
    creationTimestamp!: Date;
}
