import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { Color } from "./color";

@Entity({ name: "user" })
@Unique(["name"])
export class User {
  @PrimaryGeneratedColumn({ name: "photo_id", type: "int" })
  id!: number;

  @Column({ type: "varchar", length: 50 })
  name!: string;

  @ManyToOne(() => Color, { cascade: ["insert"] })
  @JoinColumn({ name: "color_id" })
  favouriteColor!: Color;
}
