import { createConnection, getRepository } from "typeorm";
import { User } from "../src/user";
import { Color } from "../src/color";

enum TIMESTAMP {
  value = "2020-12-08T12:14:43.891Z",
}

describe("Repository", () => {
  it("allows to save two users with the same favourite color", async () => {
    // given
    await createConnection({
      type: "sqlite",
      database: ":memory:",
      dropSchema: true,
      entities: [User, Color],
      synchronize: true,
      logging: false,
    });
    const userRepository = getRepository(User);

    const user1 = userRepository.create({
      name: "Alfred",
      favouriteColor: {
        color: "red",
        creationTimestamp: new Date(TIMESTAMP.value),
      },
    });
    const user2 = userRepository.create({
      name: "Berta",
      favouriteColor: {
        color: "red",
        creationTimestamp: new Date(TIMESTAMP.value),
      },
    });

    // when
    await userRepository.save(user1);

    // then
    const user1FromDb = await userRepository.findOne(
      { name: "Alfred" },
      { relations: ["favouriteColor"] }
    );
    expect(user1FromDb).toEqual({
      id: 1,
      name: "Alfred",
      favouriteColor: {
        id: 1,
        creationTimestamp: new Date(TIMESTAMP.value),
        color: "red",
      },
    });

    // when
    await userRepository.save(user2); // This line throws

    // then
    const user2FromDb = await userRepository.findOne(
      { name: "Berta" },
      { relations: ["favouriteColor"] }
    );
    expect(user2FromDb).toEqual({
      id: 2,
      name: "Berta",
      favouriteColor: {
        id: 1,
        creationTimestamp: new Date(TIMESTAMP.value),
        color: "red",
      },
    });
  });
});
